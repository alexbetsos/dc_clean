#testing durations
start_time <- Sys.time()

#####------------------Library, links & START FUNCTIONS--------------------#####
library(tidyverse)
library(RColorBrewer)
source("data_load.R")
end_1 <- Sys.time()

#####------------------------Cleaning Dfs----------------------------------#####

#Creates a data frame of date ranges to be used later for making sliding scale in network graph and line chart
source("Day_making.R")

####----General Cleaning from Scrape----####
dcbc$Ftir_Spectrometer <- gsub("\\Q['\\E|\\Q']\\E|\\Q'\\E", "", 
                               dcbc$Ftir_Spectrometer)

dcbc <- dcbc %>%
  #Select Data used
  select(Date, City.Town, Health_Authority,
         Site, Expected.Substance, Category, Colour,
         Texture, Fentanyl.Test.Strip, Benzo.Test.Strip,
         Ftir_Spectrometer
         ) |> 
  #name repair for strips & FTIR
  mutate(across(ends_with("Strip"), ~ str_replace_all(.x, c("^Pos$" = "True",
                                                            "^Neg$" = "False",
                                                            "Inv" = NA))))

dcbc$Ftir_Spectrometer <- sapply(strsplit(dcbc$Ftir_Spectrometer, ", "), 
                                 function(x) paste(unique(x), collapse = ", "))
#For the purposes of graphing this 'Uncertain x' aside from an
#uncertain match is likely not useful info for the general population
#it also crowds out the network graph
uncertainty <- c("Uncertain oil", "Uncertain carbohydrate",
                 "Uncertain salt","Uncertain wax")

dcbc <- dcbc %>%
#Cleaning up some things that'll make it hard to separate out the columns later
#if you use "comma space" as your separator & the drug names have commas
#you'll separate incorrectly
         mutate(Ftir_Spectrometer = str_replace_all(Ftir_Spectrometer,
                                                    c("^Neg$"= "No Library Match",
                                                      ", Neg$"= "",
                                                       "1, 4-butanediol" = "1,4-butanediol",
                                                       "2, 4-dinitrophenol"= "2,4-dinitrophenol",
                                                       "N, n-dimethylpentylone" = "N,n-dimethylpentylone",
                                                       "Negative,* *" = "",
                                                       "Positive,* *" ="")
                                                    ),
                Ftir_Spectrometer = gsub(paste0(uncertainty, collapse = "|"),
                                         "Uncertain inactive",
                                         Ftir_Spectrometer)) %>%
  mutate(Ftir_Spectrometer = str_replace_all(Ftir_Spectrometer,
                                             "Cocaine [Bb]ase", "Crack Cocaine")) %>%
  #Order matters later on so we put the strips back
  relocate(Fentanyl.Test.Strip:Benzo.Test.Strip, .after = Texture) %>%
  rowid_to_column("ID")
dcbc$Ftir_Spectrometer[dcbc$Ftir_Spectrometer == "NA"] <- NA
dcbc <- dcbc |>
  mutate(Ftir_Spectrometer= str_replace_all(Ftir_Spectrometer,
                                            c(
                                              "Neg, " = "",
                                              ", Neg, " = ""
                                            )))
#Remove all duplicates created by the steps above
#You could probably find a way to do this across rows, but this works fine
#I'm also dropping the distinction between "base" & "HCl"
#Aside from cocaine base

dcbc <- dcbc %>%
  mutate(has_base = ifelse(
    str_detect(Ftir_Spectrometer, "[Bb]ase"), 1,NA
  )) %>%
  separate(Ftir_Spectrometer,
           into = paste("FTIR", 1:6, sep = "."),
           sep = ", ", extra = "merge",
           fill = "right", remove = F) %>%
  pivot_longer(cols = c(FTIR.1:FTIR.6),
               names_to = "name",
               values_to = "value") %>%
  filter(!is.na(value)) %>%
  mutate(value = str_remove_all(value, " hcl| base"))

dc_id <- dcbc |>
  filter(!is.na(value)) %>%
  mutate(is_dup = if_else(
    duplicated(value) &
      has_base == 1,1,NA),
         .by = ID) |>
  filter(is_dup == 1) |>
  select(ID) |>
  distinct()

dcbc <- dcbc %>%
  #removes the duplicates
  mutate(value = ifelse(duplicated(value),NA,value), .by = ID) %>%
  #puts everything back
  pivot_wider() %>%
  mutate(is_dup = ifelse(ID %in% dc_id$ID, 1,0)) |>
  unite("Ftir_Spectrometer2", FTIR.1:FTIR.6, 
        sep = "_", na.rm = T) %>%
  separate(Ftir_Spectrometer2, 
           into = paste("FTIR", 1:6, sep = "."),
           sep = "_")

rm(dc_id)


dcbc <- dcbc %>%
  mutate(across(ends_with("Strip"),~ ifelse(str_detect(.x, "True|False"),
                                            .x, NA)))
                                            

op2 <- c(unique(dcbc$Expected.Substance[grep("[Ff]ent", dcbc$Expected.Substance)]),
         unique(dcbc$Expected.Substance[grep("Her", dcbc$Expected.Substance)]),
         "Down (Unknown Opioid)", "Oxycodone", "Methadone", "Morphine", "Heroin HCl",
         "Hydromorphone",
         "6-Monoacetylmorphine", "Oxycodone HCl", "Fentanyl HCl", "Fentanyl or Analog")
####---Major Change----####
#This section could be rewritten

dcbc2 <- dcbc |> 
  add_column(FTIR.7 = "a", .after = "FTIR.6") |>
  add_column(FTIR.8 = "a", .after = "FTIR.7") |>
  mutate(fent.p = if_else(
    str_detect(Ftir_Spectrometer, "[Ff]ent")|
      Fentanyl.Test.Strip == "True", 1,0,
    missing = 0
  ),
  Benzo.Test.Strip = case_when(
    Benzo.Test.Strip == "True" ~ 1,
    Benzo.Test.Strip == "False" ~ 0,
    is.na(Benzo.Test.Strip) ~ NA
  ))
dcbc2$FTIR.7[dcbc2$FTIR.7 == "a"] <- NA
dcbc2$FTIR.8[dcbc2$FTIR.8 == "a"] <- NA

#####---------------------START MAKING NEW COLUMNS-------------------------#####

# Finds all the benzos & fent to create a count
# First find the start & end variables
ftnum <- c(match("FTIR.1",names(dcbc2)), match("FTIR.8",names(dcbc2)))

benzo.match <- unlist(dcbc2[,ftnum]) %>%
  .[grepl("epam$|olam$", .)]
benzo.match <- unique(benzo.match)
dcbc2$has.benzo <- 0


#Finds all benzo positive strips which do not have positive benzo responses

dcbc2$has.benzo[dcbc2$FTIR.1 %notin% benzo.match & dcbc2$Benzo.Test.Strip ==1 &
                  dcbc2$FTIR.2 %notin% benzo.match & dcbc2$FTIR.3 %notin% benzo.match &
                  dcbc2$FTIR.4 %notin% benzo.match & dcbc2$FTIR.5 %notin% benzo.match &
                  dcbc2$FTIR.6 %notin% benzo.match] <- 1


#loop finds the positive fent and benzo strips where they are not listed
#Answer from stackoverflow: https://stackoverflow.com/a/64994253/7263991

dcbc2 <- as.data.frame(dcbc2) #tidyverse functions above have a pernicious habit of fucking up this base R code
cols_have <- paste("FTIR", 1:8, sep=".")
colidx <- names(dcbc2) %in% cols_have
for(i in 1:2){
  if(i == 1){
    ci <- seq_along(dcbc2[colidx])[max.col(cbind(is.na(dcbc2[colidx]), TRUE), ties.method = "first")]
    ri <- rowSums(sapply(dcbc2[colidx], `%in%`, op2)) == 0 &  dcbc2$fent.p == 1 & !grepl("[Ff]ent", dcbc2$FTIR.1)
    dcbc2[colidx][na.omit(cbind(which(ri), ci[ri]))] <- "Fent Strip"
  }
  if(i==2){
    ci <- seq_along(dcbc2[colidx])[max.col(cbind(is.na(dcbc2[colidx]), TRUE), ties.method = "first")]
    ri <- rowSums(sapply(dcbc2[colidx], `%in%`, benzo.match)) == 0 &  dcbc2$has.benzo == 1
    dcbc2[colidx][na.omit(cbind(which(ri), ci[ri]))] <- "Benzo Strip"
  }
  
}
#In order to compute the data all FTIR.2 cannot be empty - no cuts, represents when no adulterants were found
dcbc2$FTIR.2[which(is.na(dcbc2$FTIR.2))] <- paste("No Cuts\n", dcbc2$FTIR.1[which(is.na(dcbc2$FTIR.2))], sep = "")
dcbc2$FTIR.1[which(grepl("No Cuts", dcbc2$FTIR.2))] <- dcbc2$FTIR.2[which(grepl("No Cuts", dcbc2$FTIR.2))]
dcbc2 <- dcbc2[dcbc2$FTIR.1 != "",] %>%
  mutate(across(where(is.character), str_trim))
dcbc2$Week.val <- poss.w$Days2[match(dcbc2$Date, poss.w$Days)]
rownames(dcbc2) <- NULL
hb2 <- paste(benzo.match, collapse = "|")
dcbc2$has.benzo2 <- 0
dcbc2 <- dcbc2 %>%
  mutate(has.benzo2 = ifelse(str_detect(Ftir_Spectrometer, hb2) == TRUE |
                               Benzo.Test.Strip == 1, 1,0))
dcbc2$has.benzo2[which(is.na(dcbc2$has.benzo2))] <- 0


####----------------------String replacement!-------------------------------####

dcbc2 <- dcbc2 %>%
  mutate(Expected.Substance = str_replace_all(Expected.Substance, 
                                              c("Down \\Q(Unknown Opioid)\\E|Fentanyl"="Fentanyl/Down",
                                                "^Cocaine$" = "Cocaine HCl"
                                                
                                                
                                              ))
         ) %>%
  pivot_longer(cols = matches("FTIR.[0-9]"),
               names_to = "name",
               values_to = "value") %>%
  filter(!is.na(value)) %>%
  mutate(value = str_replace_all(value,c("Fentanyl HCl" = "Fentanyl or Analog", 
                                         "Dextromethorphan" = "DXM",
                                         "6-Monoacetylmorphine" = "6-MAM", "Surcrose" = "Sucrose", 
                                         "Polyethylene Glycol" = "PEG", "Lysergic Acid Diethylamide" = "LSD", 
                                         
                                         "etizolam" = "Etizolam"
  ))) %>%
  mutate(value = str_replace_all(value,"Sugar Possibly [A-Za-z]+", "Sugar Uncertain")) %>%
  mutate(value = str_replace_all(value, "Heroin \\Q(Trace)\\E", "Heroin"))
#A little bit of duplication cleaning

dcbc2 <- dcbc2 |>
  mutate(value = str_replace_all(value, "Fentanyl$", "Fentanyl or Analog")) 

  
#This is just to speed things up



dcbc2$value <- str_to_title(dcbc2$value)

dcbc2 <- dcbc2 %>%
  filter(!is.na(Ftir_Spectrometer)|value != "No Cuts\nNa")

source("Drug_Class2.R")

#Subset the n of Expected Substances to what will be used

interest <- c(unique(
  dcbc2$Expected.Substance[grepl("[Ff]ent|[Mm]orph",
                                 dcbc2$Expected.Substance)]), 
  "Hydromorphone",
  "Methamphetamine", "Ketamine", "Cocaine HCl", 
  "Crack Cocaine", "MDMA", "Alprazolam", "Etizolam",
  unique(dcbc2$Expected.Substance[grepl("zene", dcbc2$Expected.Substance)]))

int_expect <- plyr::count(dcbc2$Expected.Substance[dcbc2$Date >as.Date("2020-01-01")]) %>%
  dplyr::filter(freq > 20, x != "Unknown") %>%
  arrange(-freq)
main_drugs <- unique(c(interest, int_expect[c(1:20),1]))
#Selects all opioids - fent
dc_nofent <- dcbc2 %>%
  filter(Category == "Opioid" & !str_detect(Expected.Substance, "[Ff]ent"))
dc_nofent$Expected.Substance <- "All Opioids Minus Fentanyl/Fentanyl Analogues"
dc_nofent$ID <- max(dcbc2$ID)+dc_nofent$ID

dcbc2 <- dcbc2 %>%
  filter(Expected.Substance %in% main_drugs) %>%
  bind_rows(dc_nofent) 


####------------------------MAKING EDGELIST---------------------------------####
edge_start <- Sys.time()
dcbc4 <- dcbc2 %>%
  select(ID,Week.val, Expected.Substance, City.Town) %>%
  distinct(ID, .keep_all = TRUE)

#new
dcbc3 <- dcbc2%>%
  select(ID, value) %>%
  nest(data=(value), .by = ID)  %>%
  mutate(data) %>%
  mutate(pairs=map(data, ~as_tibble(as.data.frame(t(combn(.$value, 2))),
                                    column_name = c("V1", "V2")), 
                   .name_repair= "unique", .keep))

Edges <- dcbc3 %>%
  unnest(pairs) %>%
  select(-data) %>%
  inner_join(dcbc4, by = "ID") %>%
  select(ID, Week.val, Expected.Substance, City.Town, V1, V2)
rm(dcbc3, dcbc4)

edge_end <- Sys.time()
print(paste("edge time", edge_end-start_time))
####---------Creates df for classification and the colour palette-----------####

node_col <- data.frame(Names = unique(dcbc2$value)) %>%
  rowid_to_column("ID")

node_col$Classification <- tod$Classification[match(node_col$Names,tod$Names)]
node_col$Classification <- tod$Classification[match(node_col$Names,tod$str_t)]

node_col$Classification[which(grepl("lam$|pam$", node_col$Names))] <- "Benzodiazepine"
#Since the dictionary is handwritten  this next line just makes sure it won't throw errors
node_col$Classification[is.na(node_col$Classification)] <- "Other or Not in Library" 
regrouped <- data.frame(ID = seq(2000, 1999+length(unique(node_col$Classification)),by=1),
                        Names = unique(unique(node_col$Classification)),
                        Classification = unique(unique(node_col$Classification)))
node_col <- rbind(node_col, regrouped)



####--------------------------Making Benzo Table----------------------------####

new.op <- unique(dcbc2$Expected.Substance[grepl("[Ff]ent|zene", 
                                                dcbc2$Expected.Substance)])
new.op <- new.op[!grepl("Minus", new.op)]

b.match2 <- paste(benzo.match, collapse = "|")
#take only the 1s i'm interested in
#Pivot wider is required otherwise you x2
benzo <- dcbc2 %>%
  filter(Expected.Substance %in% interest) |> 
  pivot_wider(names_from = name, values_from = value)
#Makes the grouped category "All Fentanyl & Fentanyl Analogues (Grouped)"
b_fent <- dcbc2 %>%
  filter(Expected.Substance %in% new.op) %>%
  pivot_wider(names_from = name, values_from = value) %>%
  mutate(Expected.Substance = "All Fentanyl & Fentanyl Analogues (Grouped)")
#Put them together
benzo <- bind_rows(benzo, b_fent)
#Makes the grouped category for the whole province
benzo.all <-benzo %>%
  mutate(City.Town = "All of BC")
benzo <- bind_rows(benzo,benzo.all) 

#Creates our totals
benzo <- benzo %>%
  mutate(B.Not.Test = ifelse(is.na(Benzo.Test.Strip) & has.benzo2 == 0, 1,0)) %>%
  select(Week.val,  City.Town, B.Not.Test, Expected.Substance, 
         fent.p, has.benzo2) %>%
  group_by(Week.val, Expected.Substance, City.Town) %>%
  summarize(Week.val = unique(Week.val), 
            City.Town = unique(City.Town),
            Expected.Substance = unique(Expected.Substance),
            fent.count = sum(fent.p),
            benzo.count = sum(has.benzo2),
            tot = n(),
            tot.not.test = n()-sum(B.Not.Test)) %>%
  ungroup()

#makes %
benzo$fent.perc <- benzo$fent.count/benzo$tot*100
benzo$benzo.perc <- benzo$benzo.count/benzo$tot*100
benzo$b.perc.na <- benzo$benzo.count/benzo$tot.not.test*100


#Makes them prettier to read
benzo <- benzo %>%
  pivot_longer(c(fent.count, benzo.count, fent.perc, 
                 benzo.perc, b.perc.na), names_to = "name",
               values_to = "Percent") %>%
  mutate(name = str_replace_all(name, 
                                c("fent.count" = "Fentanyl Count",
                                  "fent.perc"= "% Fentanyl",
                                  "benzo.perc" = "% Benzo",
                                  "benzo.count" = "Benzo Count",
                                  "b.perc.na" = "% Benzo's excluding Samples not tested"
  )))

closure <-  c("Mar 16-\nMar 22\n2020", "Mar 23-\nMar 29\n2020", "Mar 30-\nApr 05\n2020")
#Only want to include % for places where it makes sense
place.with.percent <- benzo %>%
  filter(Expected.Substance == "Fentanyl/Down") %>%
  group_by(City.Town) %>%
  summarise(med = median(tot), the_max = max(tot), the_q = quantile(tot, 0.75)) %>%
  ungroup() %>%
  filter(med < 20)

benzo <- benzo %>%
  filter((!str_detect(name, "%") & !City.Town %in% place.with.percent$City.Town) | str_detect(name, "Count") | (City.Town %notin% place.with.percent$City.Town & str_detect(name, "%")))


benzo$Percent[grepl("\\Q%\\E", benzo$name) & benzo$tot < 20] <- NA
benzo$Percent[which(benzo$Week.val %in% closure)] <- NA
benzo$Percent[which(is.na(benzo$Percent))] <- NA
benzo$Cheque <- poss.w$Cheque[match(benzo$Week.val, poss.w$Days2)]
  
benzo$Size <- ifelse(benzo$Cheque == "cheque", 6,4)
benzo <- benzo %>%
  drop_na()

dcbc <- dcbc2
colnames(dcbc)
dcbc <- dcbc |> 
  select(ID, Date,Week.val, Expected.Substance, City.Town, name, value)
edges<- Edges
poss.w <- poss.w %>%
  dplyr::filter(Days <= max(dcbc$Date)) %>%
  select(ID, Days2) %>%
  distinct(.)
end2 <- Sys.time()
print(paste("Total Time", end2-start_time))

save(dcbc, node_col, tod, edges, benzo,poss.w, file = "DC_BC.RData")
print(paste("it took", Sys.time()-start_time, "for this to be done", sep = " "))

